#!/bin/bash
dt=`date "+%F %H:%M:%S"`
dt2=`date +%Y%m%d`
log=/home/ubuntu/service/www/web/mon.txt
ifn=`sudo route | grep '^default' | grep -o '[^ ]*$'`
traffic=`sudo ifconfig  $ifn  | grep "RX bytes"`

echo "" > $log
echo "========================================================================================" >> $log
echo $dt" >>>>> Server Status ">> $log
echo "----------------------------------------------------------------------------------------" >> $log
echo $dt" Traffic:"$traffic >> $log
echo "----------------------------------------------------------------------------------------" >> $log
echo "" >> $log
free -h >> $log
echo "----------------------------------------------------------------------------------------" >> $log
echo "" >> $log
ss -s >> $log
echo "----------------------------------------------------------------------------------------" >> $log
echo "" >> $log
top -b -n 1 | head -n 55 >> $log
