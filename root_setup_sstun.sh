#!/bin/bash
. ./functions.sh

# add user for SSR
add_ubuntu_user

# setup timezone to china
setup_timezone

# make a directory for pkg
make_pkg_directory

# copy pre install files
copy_pre_pkg_files

# install services
if [ "$is_ubuntu" = "1" ]; then
init_ubuntu_sstun_service
install_apt_sstun_pkgs
fi

if [ "$is_centos" = "1" ]; then 
init_centos_sstun_service
install_yum_sstun_pkgs
fi

# init crontab
init_crond_sstun

# firewall
enable_ufw

# copy something 
copy_post_pkg_files

# block torrent
block_bittorrent

su - ubuntu
